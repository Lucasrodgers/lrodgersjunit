package lucasJunit;

public class UserInfo {

    public static class johnInfo{

        //This is John's information
        String firstName = "John";
        String lastName = "Schwartz";
        int[] favEvenNumbers = {2, 4, 6, 8, 10, 14};
        int favOddNumber = 15;
        String[] familyFirstNames = {"Timmy", "John", "Miguel", "Richard"};
        String[] familyLastNames = {"Reid", "Wagner", "Mueller", "Ferguson"};
        String nameOfChildren = null;
        int numOfChildren = 0;
        boolean isMarried = true;
}

    public static String userJohn() {

        //Brings in John's information and saves it as a new object.
        johnInfo newInfo = new johnInfo();

        //prints out favorite even numbers
        System.out.println("\nHis favorite even numbers are: ");
        for (int i = 0; i < 6; i++) {
            System.out.print(newInfo.favEvenNumbers[i] + " ");
        }
        //prints out his favorite odd number
        System.out.println("\n\nHis favorite odd number is: \n" + newInfo.favOddNumber);

        //prints out a list of family members
        System.out.println("\nSome of the members of his family are: ");
        for (int i = 0; i < 4; i++) {
            System.out.println(newInfo.familyFirstNames[i] + " " + newInfo.familyLastNames[i]);
        }

        //prints out marital status
        System.out.println("\n");
        if (newInfo.isMarried) {
            System.out.println(newInfo.firstName + " " + newInfo.lastName +
                    " is happily married");
        } else {
            System.out.println(newInfo.firstName + " " + newInfo.lastName +
                    " is still seeking the right person");
        }
        //Prints out the number of children
        System.out.println("\nHe has " + newInfo.numOfChildren + " children");

        return "";
    }
    public static void main(String[] args) {
        //Prints out John's info
        System.out.println(userJohn());
    }
}

