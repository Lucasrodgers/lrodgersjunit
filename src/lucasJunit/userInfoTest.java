package lucasJunit;

import org.junit.Test;
import static org.junit.Assert.*;

public class userInfoTest {
    public static class AssertTests {

       UserInfo.johnInfo johnInfoTest = new UserInfo.johnInfo();

        //This makes sure the number array matches what is expected
        @Test
        public void assertArrayEqual(){
            //This confirms that the even numbers match desired output.
            int[] expectedOutput = {2,4,6,8,10,14};
            int[] methodOutput = johnInfoTest.favEvenNumbers;
            assertArrayEquals("Error: Numbers do not match", expectedOutput, methodOutput);
        }

        //This is set to make sure there is an odd number in this variable
        @Test
        public void assertIsFalse(){
            assertFalse("Number is not even",(johnInfoTest.favOddNumber % 2 == 0));
        }

        //This test makes sure there is a value in an object
        @Test
        public  void assertIsNotNull(){
           assertNotNull("There should be data here", johnInfoTest.familyFirstNames[3]);
        }

        //This test makes sure no too objects are the same
        @Test
        public void notTheSame(){
            assertNotSame("First names and last names need to be in different strings"
                    ,johnInfoTest.familyFirstNames, johnInfoTest.familyLastNames);
        }

        //This test checks to see if an object is null
        @Test
        public void isEmpty(){
            assertNull("This needs to be empty", johnInfoTest.nameOfChildren);
        }

        //This test checks to see if a boolean value is true.
        @Test
        public void isTrue(){
            assertTrue("This should be true", johnInfoTest.isMarried);
        }

    }
}